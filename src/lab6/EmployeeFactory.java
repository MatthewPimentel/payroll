package lab6;

public class EmployeeFactory {
    private static EmployeeFactory empFactory;
    
    private EmployeeFactory(){
        
    }
    
    public static EmployeeFactory getInstance(){
        if(empFactory == null){
            empFactory = new EmployeeFactory();
        }
        return empFactory;
    }
    
    public Employee getEmployee(EmployeeTypes type, double wage, double hours, double bonus){
        Employee emp = null;
        switch (type){
            case EMPLOYEE:
                emp = new Employee(wage,hours);
                break;
            case MANAGER:
                emp = new Manager(wage,hours,bonus);
                break;
        }
        return emp;
    }
}
