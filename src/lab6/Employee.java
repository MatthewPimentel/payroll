/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

/**
 *
 * @author Matt
 */
public class Employee {
    private double wage;
    private double hours;

    Employee(double wage, double hours){
        this.wage = wage;
        this.hours = hours;
    }
    
    public double calculatePay(){
        return wage * hours;
    }
    
}

