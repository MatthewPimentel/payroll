package lab6;

public class PayrollSimulation {

    public static void main(String[] args) {
        EmployeeFactory factory = EmployeeFactory.getInstance();

        Employee employee = factory.getEmployee(EmployeeTypes.EMPLOYEE, 25, 40, 0);
        Employee manager = factory.getEmployee(EmployeeTypes.MANAGER, 30, 40, 700);
        System.out.println("Employee total wage: " + employee.calculatePay());
        System.out.println("Manager total wage: " + manager.calculatePay());
    }

}
