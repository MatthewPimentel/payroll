/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

/**
 *
 * @author Matt
 */
public class Manager extends Employee {
    private double bonus;
    
    Manager(double wage, double hours, double bonus){
        super(wage,hours);
        this.bonus = bonus;
    }
    
    public double calculatePay(){
        return super.calculatePay() + bonus;
    }
}
    
